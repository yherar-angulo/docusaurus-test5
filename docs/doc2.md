---
id: doc2
title: Solución ITAÚ JiraMobile
sidebar_label: ITAÚ
---


# BANCO ITAÚ


### NECESIDAD


El presente documento se enfoca en la necesidad de BANCO ITAÚ de contar con el servicio de Jira disponible para el acceso de distintos dispositivos desktop y mobile por lo cual, se requiere tener un endpoint público (seguro) para poder realizar la gestión de incidentes (como se conoce en Jira) sin la necesidad física de estar en la red corporativa por la flexibilidad que esto permite, teniendo en cuenta que no pueda compartirse información sensible mediante la carga y descarga de archivos en los incidentes (user story, bug, etc).

El detalle de la solución propuesta se presenta a continuación, habiéndose realizado pruebas de laboratorio replicando la situación en un entorno similar al existente en BANCO ITAÚ.


### ANÁLISIS REALIZADO


El análisis fue realizado en un entorno de laboratorio con la versión de Jira 6.2, NGINX 1.17.10 y openLDAP v2.4.41.

Todos los servicios ejecutándose sobre una red interna (privada) y el reverse proxy (nginx) con una regla NAT de una IP pública a la IP privada. El tráfico se direccionó hacia la instancia de Jira y se filtró exitosamente los request definidos en la documentación oficial de Atlassian para los GET y POST de attachments (se enviaron response code 401).

Ejemplo de configuración aplicada:

<pre><code>
server {
listen 80;
large_client_header_buffers 4 32k;
proxy_set_header HOST $host;
proxy_set_header X-Forwarded-Proto $scheme;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

location ~* /AttachTemporaryFile? {
return 401;
}

location ~* /attachment/ {
return 401;
}

location / {
proxy_pass http://xxx.xx.xx.xx:8082/;
}

error_page 500 502 503 504 /50x.html;
location = /50x.html {
root /usr/share/nginx/html;
}
}
</code></pre>


Si bien este no es el escenario propuesto, sirvió para prueba de laboratorio e identificar posibilidad de realizarlo de forma productiva.

Información de la prueba efectuada, puede verse en los siguientes links:

https://www.loom.com/share/f2227ff096a64d469d5cd40f47bae24e
https://www.loom.com/share/963ee83109c447628b91a4e56dbc3447

Para ambos videos la clave es: semperti2020

### SOLUCIÓN PROPUESTA


En base a la documentación oficial de Atlassian, la forma de exponer el servicio de Jira se realiza según lo definido en el siguiente gráfico:


![alt-text](assets/1-gonza.png)


*Fuente*: https://community.atlassian.com/t5/Jira-Align-articles/Jira-Align-amp-Jira-Connectivity/ba-p/1284602

Esto representa una solución similar a la utilizada en la prueba de laboratorio, donde se identificaron diferencias en las APIs de la prueba (Jira v 6.2) y las del entorno productivo de BANCO ITAÚ (Jira v 8.3.3).

Las APIs sobre las cuales deben realizarse las restricciones de carga y descarga de archivos, pueden encontrarse también en la documentación oficial de Atlassian que está identificada por cada una de las versiones existentes.


*Dicha documentación se puede encontrar en*:

https://docs.atlassian.com/software/jira/docs/api/REST/8.3.3/

Para el diseño de la solución se han elaborado 2 alternativas, una aplicando las restricciones a nivel Firewall y otra haciéndolo según las pruebas efectuadas en un Reverse Proxy, entendiendo que quizás lo más aconsejable y para evitar agregar puntos de falla, sería realizarlo directamente en el Firewall con las reglas correspondientes.


• Alternativa 1 – Sin Reverse Proxy


![alt-text](assets/2-gonza.png)


• Alternativa 2 – Con Reverse Proxy


![alt-text](assets/3-gonzq.png)



En ambos casos, no vemos necesidad de exponer un AD ya que la integración debería realizarse internamente desde Jira para permitir la autenticación de los usuarios.

Las acciones necesarias para esta solución serían:

- Habilitación de un endpoint de Jira y el acceso desde el mismo desde el FW para direccionar el tráfico.

- Creación de un dominio para su acceso (ej: jira.itau.com.ar)

- Gestión de certificados para su acceso de forma segura

- Configuración de reglas de “filtrado” de los request para GET y POST attachements (en FW o Reverse Proxy)

No obstante, antes de realizar todas estas acciones, se recomienda realizar una prueba controlada donde pueda evidenciarse el correcto uso de la aplicación (Jira) tal como se pudo efectuar en su instancia de laboratorio.


#### REFERENCIAS
https://community.atlassian.com/t5/Jira-Align-articles/Jira-Align-amp-Jira-Connectivity/ba-p/1284602

https://docs.atlassian.com/software/jira/docs/api/REST/8.3.3/

https://docs.atlassian.com/software/jira/docs/api/REST/
