---
id: doc1
title: Instalacion de Ralph
s
debar_label: Ralph 03
---


*Tiempo de lectura: 10 minutos*


## Pasos


*Nota: Los pasos a continuación se pueden ejecutar en cualquier instalación limpia de Ubuntu 18.04.*


- 1.-  Se agregan las Key del repositorio de Ralph 03:

<pre><code>
sudo apt-key adv --keyserver  hkp://keyserver.ubuntu.com:80 --recv-keys E2D0F3764B54797F
sudo sh -c "echo 'deb https://dl.bintray.com/allegro/debng bionic main' >  /etc/apt/sources.list.d/ralph.list"
</code></pre>


- 2.- Ralph 03 requiere para su funcionamiento Mysql y un servidor nginx, se instalan dichos paquetes y el paquete de ralph 03:

<pre><code>
sudo apt-get update
sudo apt-get install mysql-server nginx ralph-core
</code></pre>


- 3.- En la dirección /etc/nginx/sites-available/default se configura el archivo de nginx, colocando:

<pre><code> 
server {
    listen 80;
    client_max_body_size 512M;

    proxy_set_header Connection "";
    proxy_http_version 1.1;
    proxy_connect_timeout  300;
    proxy_read_timeout 300;

    access_log /var/log/nginx/ralph-access.log;
    error_log /var/log/nginx/ralph-error.log;

    location /static {
        alias /usr/share/ralph/static;
        access_log        off;
        log_not_found     off;
        expires 1M;
    }

    #location /media {
    #    alias /var/local/ralph/media;
    #    add_header Content-disposition "attachment";
    #}

    location / {
        proxy_pass http://127.0.0.1:8000;
        include /etc/nginx/uwsgi_params;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
 }
</code></pre>


- 4.- Se reinicia el servicio de Nginx y se crea la base de datos con usuario y sus respectivos 
permisos:

<pre><code>
sudo systemctl restart nginx.service

sudo mysql
mysql> create user 'ralph_ng'@'127.0.0.1' identified by 'ralph_ng';
mysql> grant all privileges on ralph_ng.* to 'ralph_ng'@'127.0.0.1';
mysql> create database ralph_ng;
</code></pre>


- 5.- Se crean configuraciones para Ralph 03:

<pre><code>
sudo ralphctl migrate
sudo ralphctl createsuperuser
sudo ralphctl demod- 6.- Se inicia el servicio de ralph 03:

sudo ralphctl sitetree_resync_apps
sudo systemctl enable ralph.service
sudo systemctl start ralph.service
</code></pre>


*Ralph estará funcionando en: http://localhost* 


## Ralph 03 implementado en Semperti:


##### Características de la VM:

+ Nombre de la VM en el RHEV:
ralph03
+ IP estática:
10.252.7.108
+ Sistema Operativo:
ubuntu-18.04.4-live-server-amd64
+ Tamaño de la memoria:
4024 MB
+ Total de CPUs virtuales:
2
+ User:
devops
Password:
semperti2020


##### Credenciales de Mysql:

+ user: root
+ password: semperti2020
+ user: devops
+ password: Semperti2020!
+ Nombre base de datos:
+ ralph_data_infra_semperti


##### Configuración del Ralph 03:
 
+ Dirección para acceder:
http://ralph.semperti.local/
Superadmin-ralph:
devops
Password-ralph:
Semperti2020$


##### Archivos de Registro de Ralph-03:

/var/log/ralph/ralph.log
/var/log/ralph/gunicorn.error.log
/var/log/ralph/gunicorn.access.log
/var/log/nginx/ralph-error.log
/var/log/nginx/ralph-access.log


##### Ruta del archivo de configuración: /etc/nginx/sites-available/default
- contenido:
 
<pre><code>
DATABASE_ENGINE=transaction_hooks.backends.mysql
DATABASE_HOST=localhost
DATABASE_PORT=3306
DATABASE_NAME=ralph_data_infra_semperti
DATABASE_USER=devops
DATABASE_PASSWORD=Semperti2020!
</code></pre>


*Ralph estará funcionando en: http://ralph.semperti.local/*


